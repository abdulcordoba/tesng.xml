#!/bin/bash
COOLRT=src/test/resources/coolrt.s

(spim -exception_file $COOLRT -file $1 | sed '1,/main/ d' | sed -n '2,$ p') > $2
